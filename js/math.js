function druhaMocnina(cislo) {
    return cislo * cislo;
}

function max(x, y) {
    if (x > y) {
        return x;
    }

    return y;
}

function jeParne(cislo) {
    return ((cislo % 2) === 0);
}
