function otocDolava() {
    otocDoprava();
    otocDoprava();
    otocDoprava();
}

function polozVpredu() {
    krokDopredu();
    polozZnacku();
}

function prikladUvod() {
    krokDopredu();
    krokDopredu();
    polozZnacku();
    otocDoprava();
    krokDopredu();
    krokDopredu();
    otocDoprava();
    krokDopredu();
    krokDopredu();
    polozZnacku();

    otocDoprava();
    otocDoprava();
    otocDoprava();

    krokDopredu();
    krokDopredu();

    otocDoprava();
    otocDoprava();
    otocDoprava();

    krokDopredu();
    krokDopredu();
    polozZnacku();
}

/*

jePredTebouStena()

stojisNaZnacke()

!výraz

while (výraz) {
}

*/

function pocetPolicokVStlpci() {
    let pocetPolicok = 1;

    while (!jePredTebouStena()) {
        krokDopredu();

        pocetPolicok++;
    }

    console.log(pocetPolicok);
}

function ciaraDlzky5() {
    const dlzka = 5;

    polozZnacku();

    let uzPrejdene = 1;

    while (uzPrejdene < dlzka) {
        krokDopredu();
        polozZnacku();

        uzPrejdene = uzPrejdene + 1;
    }
}

function ciaraDlzkyN_stara(dlzka)
{
    dlzka = dlzka - 1;

    while (!jePredTebouStena() && dlzka > 0) {
        polozZnacku();
        krokDopredu();

        dlzka = dlzka - 1;
    }

    polozZnacku();
}

function opakuj(kolkokrat, operacia) {
    while (kolkokrat--) {
        operacia();
    }
}

function celomVzad() {
    otocDolava();
    otocDolava();
}

function prejdiDokonca() {
    while (!jePredTebouStena()) {
        krokDopredu();
    }
}

function vyfarbiVedlajsiStlpec()
{
    otocDoprava();
    krokDopredu();
    otocDolava();
    ciaraDlzkyN(20);
}

function obrazok1() {
    let ostavaCiar = 10;

    while (ostavaCiar) {
        vyfarbiVedlajsiStlpec();
        celomVzad();
        prejdiDokonca();

        otocDolava();

        if (jePredTebouStena()) {
            break;
        } else {
            krokDopredu();
            otocDolava();
        }

        ostavaCiar = ostavaCiar - 1;
    }
}


function ukladajDoKonca() {
    while (!jePredTebouStena()) {
        polozAkSaDa();
        krokDopredu();
    }

    polozAkSaDa();
}

function krokDopreduAkSaDa() {
    if (!jePredTebouStena()) {
        krokDopredu();
    }
}

function polozAkSaDa() {
    if (!stojisNaZnacke()) {
        polozZnacku();
    }
}

function obrazok2() {
    let jeHotovo = false;

    prejdiDokonca();
    otocDoprava();

    while (!jeHotovo) {
        otocDoprava();
        krokDopredu();
        otocDolava();
        ukladajDoKonca();
        celomVzad();
        prejdiDokonca();
        otocDolava();

        if (jePredTebouStena()) {
            jeHotovo = true;
        } else {
            krokDopredu();
            otocDolava();
        }
    }
}

function urobNKrokov(ostavaPrejst) {
    while (ostavaPrejst > 0) {
        krokDopreduAkSaDa();
        ostavaPrejst = ostavaPrejst - 1;
    }
}

function trojuholnik1(vyska) {
    while (vyska) {
        ciaraDlzkyN(vyska);
        celomVzad();
        prejdiDokonca();
        vyska = vyska - 1;
        otocDolava();

        if (jePredTebouStena()) {
            break;
        }

        krokDopredu();
        otocDolava();
    }
}

function trojuholnik2(vyska) {
    let prazdnychPolicok = 0;

    while (vyska) {
        urobNKrokov(prazdnychPolicok);
        ciaraDlzkyN(vyska);
        vyska = vyska - 1;
        prazdnychPolicok = prazdnychPolicok + 1;
        celomVzad();
        prejdiDokonca();
        otocDolava();

        if (jePredTebouStena()) {
            break;
        }

        krokDopredu();
        otocDolava();
    }
}

function ciaraDlzkyN(ostavaPrejst) {
    while (ostavaPrejst > 0) {
        if (!stojisNaZnacke()) {
            polozZnacku();
        }

        if (!jePredTebouStena() && (ostavaPrejst > 1)) {
            krokDopredu();
        }

        ostavaPrejst = ostavaPrejst - 1;
    }
}

const stvorecSOtvorom = function (dlzkaStrany) {
    ciaraDlzkyN(dlzkaStrany);
    otocDoprava();
    ciaraDlzkyN(dlzkaStrany);
    otocDoprava();
    ciaraDlzkyN(dlzkaStrany);
    otocDoprava();
    ciaraDlzkyN(dlzkaStrany - 2);
};

const spirala = function () {
    let dlzka = 5;

    while (dlzka > 0) {
        stvorecSOtvorom(dlzka);
        
        otocDoprava();
        krokDopredu();

        if (stojisNaZnacke()) {
            break;
        }

        polozZnacku();
        krokDopredu();
        
        dlzka = dlzka - 4;
    }
};

document.addEventListener("DOMContentLoaded", function (event) {
    
});
